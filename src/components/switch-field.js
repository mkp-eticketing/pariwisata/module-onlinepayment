import React from "react"
import { Switch, Box, Typography } from "@mui/material";

const SwitchField = ({
    label,
    setValue,
    required = false,
    selectedValue,
    errorMessage = "Merchant wajib diisi",
    isError = false,
    ...other
}) => {
    const handleChange = (_event, values) => {
        setValue(values);
    };

    return (
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1, width: "100%" }}>
            <Box sx={{ display: "flex", flexDirection: "row", gap: 1, width: "100%", alignItems: "center" }}>
                <Typography color={"rgb(71 85 105)"} fontSize="0.875rem" fontWeight={"600"} height={25}>{label} <span style={{ color: 'red' }}>{`${required ? "*" : ""}`}</span></Typography>
                <Switch
                    checked={selectedValue}
                    onChange={handleChange}
                    inputProps={{ 'aria-label': 'controlled' }}
                />
            </Box>
            <Typography fontSize={12} color="red">{isError && errorMessage}</Typography>
        </Box>
    )
}
export default SwitchField;
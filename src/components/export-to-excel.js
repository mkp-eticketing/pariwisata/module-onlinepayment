import * as XLSX from "xlsx";
import moment from "moment";
import CustomButton from "./custom-button";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { useState } from "react";
import { getUserAccountList } from "../services/pasar/userPasar";
import React from "react";

const ExportToExcel = ({ filename, sheetname, ouCodeSelected, color }) => {
  const [exportLoading, setExportLoading] = useState(false);

  const handleFetch = () => {
    setExportLoading(true);

    getUserAccountList({ outletCode: ouCodeSelected })
      .then((res) => {
        if (res.result) {
          // List of properties to keep
          const propertyMap = {
            accountName: " Customer Name",
            accountIdentityType: "Account ID Type",
            accountIdentity: "Account ID",
            accountPhoneNumber: "Phone Number",
            accountAddress: "Address",
            accountEmail: "Email",
            corporateName: "Merchant Name",
            createdAt: "Registered Date",
          };
          // modify the data to keep only the desired columns
          const modifiedData = res.result.map((obj) => {
            const newObj = {};
            for (const [originalProp, customProp] of Object.entries(
              propertyMap
            )) {
              if (obj.hasOwnProperty(originalProp)) {
                newObj[customProp] = obj[originalProp];
              }
            }

            return newObj;
          });

          // change the created at column from modifiedData into the desired format
          const modifiedDateFormat = modifiedData.map((data) => ({
            ...data,
            "Registered Date": moment(data["Registered Date"]).format(
              "Do MMMM YYYY, h:mm:ss"
            ),
          }));
          
          // converting to excel
          exportToExcel(modifiedDateFormat);
        } else {
          exportToExcel([]);
        }
        setExportLoading(false);
      })
      .catch((e) => {
        setExportLoading(false);
        exportToExcel([]);
      });
  };

  const exportToExcel = (data) => {
    // Format the data into a worksheet
    const ws = XLSX.utils.json_to_sheet(data);

    // Create a workbook with a single sheet
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, sheetname || "Sheet1");

    XLSX.writeFile(wb, filename, { compression: true });
  };

  return (
    <CustomButton
      onClick={handleFetch}
      startIcon={<FileDownloadIcon size="20px" />}
      name="Export&nbsp;"
      loading={exportLoading}
      color={color}
    >
      Download
    </CustomButton>
  );
};

export default ExportToExcel;

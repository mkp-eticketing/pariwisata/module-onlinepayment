import apiHelper from "../../utils/payment/axios";

const getPaymentList = (data, isDevelop) => apiHelper.POST_WITH_AUTH("/corporate/partner", data, {}, isDevelop)
const getInquiryBooking = (data, token, isDevelop, header = {}) => apiHelper.POST_OTE("/v2/bookingsignature/inquiry", data, { ...header }, token, isDevelop)
const getConfirmationBooking = (data, token, isDevelop) => apiHelper.POST_OTE("/bookingv2/confirmation", data, {}, token, isDevelop)
const getPreInquiryBooking = (data, token, isDevelop, header = {}) => apiHelper.POST_OTE("/v2/bookingsignature/preinquiry", data, { ...header }, token, isDevelop)

export {
    getPaymentList,
    getInquiryBooking,
    getConfirmationBooking,
    getPreInquiryBooking
}
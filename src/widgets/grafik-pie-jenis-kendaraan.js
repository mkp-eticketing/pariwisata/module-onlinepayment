import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official';
import React from 'react';
require('highcharts/indicators/indicators')(Highcharts)
require('highcharts/indicators/pivot-points')(Highcharts)
require('highcharts/indicators/macd')(Highcharts)
require('highcharts/modules/exporting')(Highcharts)
require('highcharts/modules/map')(Highcharts)

const GrafikPieJenisKendaraan = ({ data = [] }) => {
    const options = {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}% of which total vehicle types: {point.origin}</b>'
        },
        subtitle: {
            text: ''
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                allowPointSelect: true,
                color: "#5859D4",
                cursor: 'pointer',
                dataLabels: [{
                    enabled: true,
                    distance: 20
                }, {
                    enabled: true,
                    distance: -40,
                    format: '{point.percentage:.1f}%',
                    style: {
                        fontSize: '1.2em',
                        textOutline: 'none',
                        opacity: 0.7
                    },
                    filter: {
                        operator: '>',
                        property: 'percentage',
                        value: 10
                    }
                }]
            }
        },
        series: [
            {
                name: 'Percentage',
                colorByPoint: true,
                data: data
            }
        ]
    }

    return (
        <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
        />
    )
}

export default GrafikPieJenisKendaraan;
export const payloadDummy = {
    "header": {
      "merchantKey": "Zpr-ECGCPXa7BPSNtu-ElMCx_ww=|hEAxY0RR9WXDtCQb_xWg1sj31l4UH4yIg4YLYrlZS6pDV-YDzKZhPD6bfbCPuv4BOZKKEQCeRcJm",
      "customerCid": "200090",
      "customerHirarki": "100001/200090",
      "customerName": "Yonathan Agen Maruti",
      "customerEmail": "yonathan@mkpmobile.com",
      "customerID": "",
      "customerPhone": "",
      "paymentBankName": "",
      "paymentBankAccount": "",
      "paymentPromoCode": "",
      "paymentDiscount": 0,
      "paymentTotal": 75,
      "paymentFee": 0,
      "billerMerchantKey": "6d826f31224c91088460f02a4e8092d72785f727d7fdcd645b2d1a3be1a290a36e923f0924702cea14b1ae1beaa0fd4f8ae54966a5f52b6059bd91fecd0b7f30",
      "billerMerchantCode": "MARUTIAGEN",
      "billerMerchantName": "MARUTI AGEN"
    },
    "detail": [
      {
        "tcCID": "300377",
        "tcVendorCode": "",
        "tcName": "MARUTI FASTBOAT",
        "tcHirarki": "100001/200090/300377",
        "bookingDateStart": "2024-11-29 00:00:00",
        "bookingDateEnd": "2024-11-29 23:59:00",
        "bookingCode": "",
        "visitorList": [
          {
            "visitorName": "ypw",
            "visitorIdType": "KTP",
            "visitorId": "1234567890123456",
            "visitorPhone": "0",
            "visitorEmail": "-",
            "visitorGender": "MALE",
            "visitorRegion": "INDONESIA",
            "wristbandCode": "",
            "visitorBirthDate": "",
            "visitorNationalityCode": "",
            "visitorNationality": "",
            "bookingList": [
              {
                "productPrice": 75,
                "productVendorCode": "",
                "productQty": 1,
                "productFee": 0,
                "productCode": "MRTSNP001",
                "productName": "[SANUR - NUSA PENIDA] ADULT DOMESTIC ",
                "uniqueProductCode": "300377_MRTSNP001"
              }
            ]
          }
        ]
      }
    ],
    "isOnsitePayment": false,
    "additionalData": null,
    "additionalFee": null,
    "additionalFeeCorporate": null,
    "pushPaymentRequest": {
      "paymentCategory": "QRIS",
      "paymentCode": "16001"
    }
  }
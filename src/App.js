import logo from './logo.svg';
import './App.css';
import PaymentModal from './pages/payment/modal/payment-modal';
import { useState } from 'react';

function App() {
  const [open, setOpen] = useState(false)
  return (
    <div className="App">
      <button onClick={() => setOpen(true)}>bayar sekarang</button>
      <PaymentModal
        open={open}
        handleClose={() => {
          setOpen(false)
        }}
        isDevelop={false}
        token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOiIyMDAwOTAiLCJleHAiOjUzMzI1OTk1NjUsImhpcmFya2kiOiIxMDAwMDEvMjAwMDkwIiwiaWQiOjE2NzQ0LCJuaWsiOiIiLCJwZXJzb25JRCI6InlvbmF0aGFuQG1rcG1vYmlsZS5jb20iLCJwZXJzb25OYW1lIjoiWW9uYXRoYW4gQWdlbiBNYXJ1dGkiLCJwZXJzb25UeXBlIjoiY3VzdG9tZXIiLCJwaG9uZSI6IiJ9.k6tj_EJR-wq2euOoPdYJ98910_SBI4biY457A7tUa3U"
      />
    </div>
  );
}

export default App;

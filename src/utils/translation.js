import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18n.use(LanguageDetector)
    .use(initReactI18next)
    .init({
        fallbackLng: "id",
        debug: true,
        returnObjects: true,
        resources: {
            en: {
                modal: {
                    metodePembayaran: "Payment Method",
                    pilihPembayaran: "Select Payment Method",
                    detailPesanan: "Order Details",
                    namaPemesan: "Customer Name",
                    emailPemesan: "Customer Email",
                    totalPembayaran: "Total Payment",
                    tanggalBooking: "Booking Date",
                    detailPengunjung: "Visitor Details",
                    pengunjung: "Visitor",
                    nama: "Name",
                    daftarPesanan: "Order List",
                    lanjutkanPembayaran: "Proceed to Payment",
                    waktuPembayaran: "Please make payment before the date",
                    prosesPembayaran: "Payment is being processed",
                    salinBerhasil: "Successfully copied!",
                    bayar: "Pay",
                    sekarang: "Now",
                    menungguPembayaran: "Waiting for Payment",
                    errorPilihPembayaran: "Please select a payment system!",
                },
            },
            id: {
                modal: {
                    metodePembayaran: "Metode Pembayaran",
                    pilihPembayaran: "Pilih Metode Pembayaran",
                    detailPesanan: "Detail Pesanan",
                    namaPemesan: "Nama Pemesan",
                    emailPemesan: "Email Pemesan",
                    totalPembayaran: "Total Pembayaran",
                    tanggalBooking: "Tanggal Booking",
                    detailPengunjung: "Detail Pengunjung",
                    pengunjung: "Pengunjung",
                    nama: "Nama",
                    daftarPesanan: "Daftar Pesanan",
                    lanjutkanPembayaran: "Lanjutkan Pembayaran",
                    waktuPembayaran: "Silahkan melakukan pembayaran sebelum tanggal",
                    prosesPembayaran: "Pembayaran sedang di proses",
                    salinBerhasil: "Berhasil di salin!",
                    bayar: "Bayar",
                    sekarang: "Sekarang",
                    menungguPembayaran: "Sedang Menunggu Pembayaran",
                    errorPilihPembayaran: "Silahkan pilih sistem pembayaran!",
                },
            },
        },
    });

export default i18n;

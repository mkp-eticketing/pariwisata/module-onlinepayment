import axios from 'axios';
import { Base64 } from "js-base64";

const username = "mkpmobile";
const password = "mkpmobile123";
axios.defaults.headers.common['Content-Type'] = "application/json";

const POST_WITH_AUTH = async (path = "", data, headers = {}, isDevelop = true) => {
    let authString = username + ":" + password
    let promise = null;
    if (isDevelop) {
        promise = new Promise((resolve, reject) => {
            axios.post("https://sandbox.mkpmobile.com/api/apps2payonline/public" + path, data, {
                headers: {
                    ...headers,
                    'Authorization': "Basic " + Base64.btoa(authString)
                }
            }).then((res) => {
                if (res.status === 200 || res.status === 201) {
                    resolve(res.data)
                }
            }).catch(async (error) => {
                reject(error);
            })
        })
    } else {
        promise = new Promise((resolve, reject) => {
            axios.post("https://apipayment.mkpmobile.com/apps2payonline/public" + path, data, {
                headers: {
                    ...headers,
                    'Authorization': "Basic " + Base64.btoa(authString)
                }
            }).then((res) => {
                if (res.status === 200 || res.status === 201) {
                    resolve(res.data)
                }
            }).catch(async (error) => {
                reject(error);
            })
        })
    }
    return promise;
}

const POST_OTE = async (path = "", data, headers = {}, token, isDevelop) => {
    let promise = null;
    if (isDevelop) {
        promise = new Promise((resolve, reject) => {
            axios.post("https://sandbox.mkpmobile.com/api/onlineticketing" + path, data, {
                headers: {
                    ...headers,
                    'Authorization': "Bearer " + token
                }
            }).then((res) => {
                if (res.status === 200 || res.status === 201) {
                    resolve(res.data)
                }
            }).catch(async (error) => {
                reject(error);
            })
        })
    } else {
        promise = new Promise((resolve, reject) => {
            axios.post("https://apipayment.mkpmobile.com/onlineticketing" + path, data, {
                headers: {
                    ...headers,
                    'Authorization': "Bearer " + token
                }
            }).then((res) => {
                if (res.status === 200 || res.status === 201) {
                    resolve(res.data)
                }
            }).catch(async (error) => {
                reject(error);
            })
        })
    }
    return promise;
}

export default {
    POST_WITH_AUTH,
    POST_OTE
};
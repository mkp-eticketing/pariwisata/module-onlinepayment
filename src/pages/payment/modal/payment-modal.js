import { Box, Dialog } from "@mui/material";
import PaymentList from "../payment-list";
import { payloadDummy } from "../../../data/payload_dummy";
import { CloseOutlined } from "@mui/icons-material";
import SelectedPayment from "../components/selected-payment";
// import i18n from "../../../utils/translation";
import React, { useEffect } from "react";
import moment from "moment";

const PaymentModal = ({
    language = null,
    getWord = () => {},
    open = false,
    handleClose = () => {},
    merchantKey = payloadDummy ? payloadDummy.header.merchantKey : "",
    callbackPayment = () => {},
    inquiryPayload = payloadDummy,
    successCallback = () => {},
    errorCallback = () => {},
    isDevelop = true,
    isConfirmationPayment = false,
    paymentMethod = "",
    paymentCategory = "",
    responseInquiry = {
        apps2PaymentData: {
            vaCode: "",
            expiredPayment: "",
            qrCode: "",
            paymentUrl: "",
            expiredAt: "",
        },
        header: {
            paymentTotal: 0,
            bookingRef: "",
        },
    },
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOiIyMDAwOTAiLCJleHAiOjUzMzI1OTk1NjUsImhpcmFya2kiOiIxMDAwMDEvMjAwMDkwIiwiaWQiOjE2NzQ0LCJuaWsiOiIiLCJwZXJzb25JRCI6InlvbmF0aGFuQG1rcG1vYmlsZS5jb20iLCJwZXJzb25OYW1lIjoiWW9uYXRoYW4gQWdlbiBNYXJ1dGkiLCJwZXJzb25UeXBlIjoiY3VzdG9tZXIiLCJwaG9uZSI6IiJ9.k6tj_EJR-wq2euOoPdYJ98910_SBI4biY457A7tUa3U",
}) => {
    useEffect(() => {
        if (language) {
            moment.locale(language);
        }
    }, [language]);

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" fullWidth={true} maxWidth={"lg"}>
            <Box onClick={() => handleClose()} sx={{ display: "flex", position: "absolute", top: 2, right: 2, cursor: "pointer" }}>
                <CloseOutlined sx={{ fontSize: "2rem" }} />
            </Box>
            <Box mt={4}>
                {isConfirmationPayment ? (
                    <SelectedPayment
                        language={language}
                        getWord={getWord}
                        paymentInfo={responseInquiry}
                        successCallback={successCallback}
                        errorCallback={errorCallback}
                        paymentMethod={paymentMethod}
                        paymentCategory={paymentCategory}
                        token={token}
                        merchantKey={merchantKey}
                        handleCloseModal={handleClose}
                        isDevelop={isDevelop}
                    />
                ) : (
                    <PaymentList
                        language={language}
                        getWord={getWord}
                        merchantKey={merchantKey}
                        callbackPayment={callbackPayment}
                        inquiryPayload={inquiryPayload}
                        successCallback={successCallback}
                        errorCallback={errorCallback}
                        token={token}
                        handleClose={handleClose}
                        isDevelop={isDevelop}
                    />
                )}
            </Box>
        </Dialog>
    );
};

export default PaymentModal;

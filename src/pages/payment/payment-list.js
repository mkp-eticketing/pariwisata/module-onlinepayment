import { Accordion, AccordionSummary, AccordionDetails, Box, Button, CircularProgress, Snackbar, Stack, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { getInquiryBooking, getPaymentList, getPreInquiryBooking } from "../../services/payment/payment";
import SelectedPayment from "./components/selected-payment";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { thousandSeparator } from "../../utils/thousand-separator";
import moment from "moment";
import { sha512 } from "js-sha512";
// import { useTranslation } from "react-i18next";

const PaymentList = ({
    language = null,
    getWord = () => {},
    merchantKey = "g5HZNCXhQjx1kdxmBgQaF2bEdg==|ZRKYlVWBiXHDOM2__C-_y3OmwZ3imTnPpON51Osv-ec1TuUDZlb5k_m2cVFHFzEjQ-x8Lk5zGMFe",
    callbackPayment = () => {},
    inquiryPayload,
    successCallback = () => {},
    errorCallback = () => {},
    handleCloseModal = () => {},
    isDevelop = true,
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOiIzMDAyOTEiLCJleHAiOjUzMTUwNjIwMjMsImhpcmFya2kiOiIxMDAwMDEvMjAwMDY2LzMwMDI5MSIsImlkIjozNTYsIm5payI6IiIsInBlcnNvbklEIjoibmF0YXNoYUBnbWFpbC5jb20iLCJwZXJzb25OYW1lIjoibmF0YXNoYSIsInBlcnNvblR5cGUiOiJjdXN0b21lciIsInBob25lIjoiIn0.93i6-hy0HnNPGBq2BQzLUbxzWqX3PEZInoHf5-JGtxI",
}) => {
    const [paymentList, setPaymentList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [paymentSelected, setPaymentSelected] = useState(null);
    const [open, setOpen] = useState(false);
    const [dataInquiry, setDataInquiry] = useState(null);
    const [paymentInfo, setPaymentInfo] = useState(null);
    const [currentInquiry, setCurrentInquiry] = useState(null);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setOpen(false);
    };

    useEffect(() => {
        if (!currentInquiry) {
            setCurrentInquiry(inquiryPayload);
        }
    }, [currentInquiry]);

    useEffect(() => {
        getInformationPayment();
    }, []);

    const getInformationPayment = () => {
        setLoading(true);
        let body = {
            merchantKey: merchantKey,
        };
        getPaymentList(body, isDevelop)
            .then((res) => {
                if (res.result) {
                    setPaymentList(res.result.paymentDetails);
                    setPaymentInfo(res.result);
                }
                setLoading(false);
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
            });
    };

    const handlePreInquiry = (payment) => {
        setLoading(true);
        let body = {
            ...inquiryPayload,
            pushPaymentRequest: {
                paymentCategory: payment.paymentCategory,
                paymentCode: payment.paymentCode,
                secretKey: payment.secretKey,
            },
        };
        let timestame = moment().toISOString();
        let signature = "POST" + "||" + "v2/bookingsignature/preinquiry" + "||" + body.header.merchantKey + "||" + sha512(JSON.stringify(body).replace(/\s/g, "").toLowerCase()) + "||" + timestame;
        let finalSignature = sha512.hmac.hex(body.header.customerCid, signature);
        getPreInquiryBooking(body, token, isDevelop, {
            "X-MKP-Signature": finalSignature,
            "X-MKP-Timestamp": timestame,
        })
            .then((res) => {
                if (res.result) {
                    setCurrentInquiry({
                        ...currentInquiry,
                        ...res.result,
                        header: {
                            ...res.result.header,
                            // "paymentDiscount": res.result.header.paymentDiscount,
                            // "paymentTotal": res.result.header.paymentTotal,
                            // "paymentFee": res.result.header.paymentServiceFee ? res.result.header.paymentServiceFee : 0,
                        },
                        additionalFeeMdr: res.result.additionalFeeMdr,
                    });
                }
                setLoading(false);
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
            });
    };

    const handlePayment = () => {
        if (!paymentSelected) {
            return handleClick();
        }
        let body = {
            ...currentInquiry,
            pushPaymentRequest: {
                paymentCategory: paymentSelected.paymentCategory,
                paymentCode: paymentSelected.paymentCode,
                secretKey: paymentInfo.secretKey,
                frontendCallbackUrl: typeof window !== "undefined" ? window.location.origin + "/history" : "",
            },
        };
        setLoading(true);
        let timestame = moment().toISOString();
        let signature = "POST" + "||" + "v2/bookingsignature/inquiry" + "||" + body.header.merchantKey + "||" + sha512(JSON.stringify(body).replace(/\s/g, "").toLowerCase()) + "||" + timestame;
        let finalSignature = sha512.hmac.hex(body.header.customerCid, signature);
        getInquiryBooking(body, token, isDevelop, {
            "X-MKP-Signature": finalSignature,
            "X-MKP-Timestamp": timestame,
        })
            .then((res) => {
                if (res.result) {
                    setDataInquiry(res.result);
                    callbackPayment(res.result);
                }
                setLoading(false);
            })
            .catch((e) => {
                setLoading(false);
                console.log(e);
            });
    };

    if (loading && !dataInquiry) {
        return (
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 2,
                    alignItems: "center",
                    justifyContent: "center",
                    minHeight: "500px",
                }}
            >
                <CircularProgress />
                <Typography fontSize={20} color={"black"}>
                    {language ? getWord("menungguPembayaran") : "Sedang Menunggu Pembayaran"}
                </Typography>
            </Box>
        );
    }

    if (dataInquiry && !loading) {
        return (
            <Box
                sx={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "column",
                    p: 2,
                    // borderStyle: "solid",
                    // borderWidth: 1,
                    // borderColor: "gray",
                    height: "100%",
                    backgroundColor: "white",
                }}
            >
                <SelectedPayment
                    handleClose={() => {
                        setDataInquiry(null);
                        setPaymentSelected(null);
                    }}
                    paymentInfo={dataInquiry}
                    successCallback={successCallback}
                    errorCallback={errorCallback}
                    paymentMethod={paymentSelected.paymentMethod}
                    paymentCategory={paymentSelected.paymentCategory}
                    paymentAlias={paymentSelected.paymentAlias}
                    token={token}
                    merchantKey={merchantKey}
                    handleCloseModal={handleCloseModal}
                    isDevelop={isDevelop}
                />
            </Box>
        );
    }

    return (
        <Box
            sx={{
                width: "100%",
                display: "flex",
                flexDirection: "column",
                p: 2,
                borderStyle: "solid",
                height: "100%",
                backgroundColor: "white",
            }}
        >
            <Stack direction={"row"} width={"100%"} alignItems={"center"} justifyContent={"space-between"}>
                <Typography fontSize={24} fontWeight={"bold"}>
                    {language ? getWord("metodePembayaran") : "Metode Pembayaran"}
                </Typography>
                <Typography fontSize={18} textAlign={"right"}>
                    {language ? getWord("pilihPembayaran") : "Pilih Metode Pembayaran"}
                </Typography>
            </Stack>
            <Stack direction={["column", "row"]} justifyContent={"space-between"} gap={2}>
                <Stack direction={"column"} gap={2} mt={4} width={["100%", "50%"]}>
                    {paymentList
                        .filter((item) => item.paymentCategory === "VIRTUAL ACCOUNT" || item.paymentCategory === "CC" || item.paymentCategory === "QRIS" || item.paymentMethod === "TUNAI TOP")
                        .map((item, index) => {
                            return (
                                <Stack
                                    key={index}
                                    direction={"row"}
                                    justifyContent={"space-between"}
                                    onClick={() => {
                                        setPaymentSelected(item);
                                        handlePreInquiry(item);
                                    }}
                                    sx={{
                                        py: 4,
                                        px: 2,
                                        borderRadius: 2,
                                        bgcolor: paymentSelected ? (paymentSelected.paymentCode === item.paymentCode ? "#B3D9FF" : "#E6E6E6") : "#E6E6E6",
                                        cursor: "pointer",
                                    }}
                                >
                                    <Typography fontSize={20} fontWeight={"bold"}>
                                        {item.paymentAlias || item.paymentCategory}
                                    </Typography>
                                </Stack>
                            );
                        })}
                </Stack>
                <Stack direction={"column"} width={["100%", "50%"]} mt={4}>
                    <Accordion defaultExpanded={true}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1-content" id="panel1-header">
                            <Typography fontSize={24} fontWeight={"bold"}>
                                {language ? getWord("detailPesanan") : "Detail Pesanan"}
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <Box
                                sx={{
                                    display: "flex",
                                    flexDirection: "column",
                                    gap: 2,
                                    maxHeight: 320,
                                    pb: 3,
                                    overflowY: "scroll",
                                }}
                            >
                                <LabelHelper label={language ? getWord("namaPemesan") : "Nama Pemesan"} value={currentInquiry && currentInquiry.header.customerName} />
                                <LabelHelper label={language ? getWord("emailPemesan") : "Email Pemesan"} value={currentInquiry && currentInquiry.header.customerEmail} />
                                {currentInquiry && currentInquiry.additionalFeeMdr && currentInquiry && currentInquiry.additionalFeeMdr.length > 0
                                    ? currentInquiry &&
                                      currentInquiry.additionalFeeMdr.map((item, index) => {
                                          return <LabelHelper key={index} label={`${item.additionalFeeDescription} (${item.additionalFeeAmountType})`} value={`Rp ${thousandSeparator(item.additionalFeeAmount)},00`} />;
                                      })
                                    : null}
                                <LabelHelper label={language ? getWord("totalPembayaran") : "Total Pembayaran"} value={`Rp ${thousandSeparator(currentInquiry && currentInquiry.header.paymentTotal)},00`} />
                                <LabelHelper label={language ? getWord("tanggalBooking") : "Tanggal Booking"} value={currentInquiry && currentInquiry.detail[0].bookingDateStart} />
                                <Typography fontSize={18} fontWeight={700} color={"black"}>
                                    {language ? getWord("detailPengunjung") : "Detail Pengunjung"}
                                </Typography>
                                <Stack direction={"column"}>
                                    {currentInquiry && currentInquiry.detail[0].visitorList.length > 0
                                        ? currentInquiry &&
                                          currentInquiry.detail[0].visitorList.map((item, index) => {
                                              return (
                                                  <Accordion key={index}>
                                                      <AccordionSummary
                                                          expandIcon={<ExpandMoreIcon />}
                                                          aria-controls="panel1-content"
                                                          id="panel1-header"
                                                          sx={{
                                                              border: "none",
                                                          }}
                                                      >
                                                          <Typography fontSize={18} fontWeight={500} color={"black"}>
                                                              {language ? getWord("pengunjung") : "Pengunjung"} {index + 1}
                                                          </Typography>
                                                      </AccordionSummary>
                                                      <AccordionDetails>
                                                          <Stack direction={"column"} gap={2}>
                                                              <LabelHelper label={language ? getWord("nama") : "Nama"} value={item.visitorName} />
                                                              <Typography fontSize={18} fontWeight={500} color={"black"}>
                                                                  {language ? getWord("daftarPesanan") : "Daftar Pesanan"}
                                                              </Typography>
                                                              <Stack direction={"column"} gap={1}>
                                                                  {item.bookingList.length > 0
                                                                      ? item.bookingList.map((booking, index) => {
                                                                            return (
                                                                                <Box
                                                                                    key={index}
                                                                                    sx={{
                                                                                        display: "flex",
                                                                                        flexDirection: "column",
                                                                                        gap: 2,
                                                                                    }}
                                                                                >
                                                                                    <LabelHelper label={`${booking.productName} x ${booking.productQty}`} value={`Rp ${thousandSeparator(booking.productPrice)},00`} />
                                                                                </Box>
                                                                            );
                                                                        })
                                                                      : null}
                                                              </Stack>
                                                          </Stack>
                                                      </AccordionDetails>
                                                  </Accordion>
                                              );
                                          })
                                        : null}
                                </Stack>
                            </Box>
                        </AccordionDetails>
                    </Accordion>
                </Stack>
            </Stack>
            <Stack direction={"row"} justifyContent={"center"}>
                <Button
                    onClick={() => handlePayment()}
                    sx={{
                        bgcolor: "#1A66FF",
                        my: 2,
                        px: 4,
                        py: 1,
                        color: "white",
                        fontWeight: "bold",
                        textTransform: "capitalize",
                        fontSize: 18,
                        ":hover": {
                            bgcolor: "#3377FF",
                        },
                    }}
                >
                    {language ? getWord("lanjutkanPembayaran") : "Lanjutkan Pembayaran"}
                </Button>
            </Stack>
            <Snackbar open={open} autoHideDuration={2000} onClose={handleClose} anchorOrigin={{ vertical: "top", horizontal: "right" }} message={language ? getWord("errorPilihPembayaran") : "Silahkan pilih sistem pembayaran!"} />
        </Box>
    );
};

const LabelHelper = ({ label, value }) => {
    return (
        <Stack direction={"row"} justifyContent={"space-between"} alignItems={"center"} flexWrap={"wrap"}>
            <Typography fontSize={18} fontWeight={500} color={"black"}>
                {label}
            </Typography>
            <Typography
                textAlign={"right"}
                fontSize={18}
                fontWeight={400}
                sx={{
                    display: "-webkit-box",
                    overflow: "hidden",
                    WebkitBoxOrient: "vertical",
                    WebkitLineClamp: 1,
                }}
                color={"black"}
            >
                {value}
            </Typography>
        </Stack>
    );
};
export default PaymentList;

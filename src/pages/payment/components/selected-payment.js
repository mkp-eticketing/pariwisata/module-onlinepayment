import { Box, Button, Stack, Typography } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { QRCodeSVG } from 'qrcode.react';
import { thousandSeparator } from "../../../utils/thousand-separator";
import moment from "moment/moment";
import Tooltip from '@mui/material/Tooltip';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { getConfirmationBooking } from "../../../services/payment/payment";
import { toPng } from 'html-to-image';
// import { useTranslation } from "react-i18next";

const SelectedPayment = ({
    language = null,
    getWord = () => {},
    paymentInfo = null,
    paymentMethod,
    paymentAlias = "",
    successCallback = () => { },
    errorCallback = () => { },
    token,
    merchantKey = "",
    paymentCategory = "",
    handleCloseModal = () => { },
    isDevelop = true
}) => {
    // const { t } = useTranslation("modal");
    const downloadRef = useRef()
    const [open, setOpen] = useState(false);
    const [_loading, setLoading] = useState(false)
    const [isSuccess, setSuccess] = useState(false);
    const [dataSuccess, setDataSuccess] = useState(null)
    const [refresh, setRefresh] = useState(Date.now())

    const handleTooltipClose = () => {
        setOpen(false);
    };
    const handleTooltipOpen = () => {
        setOpen(true);
    };
    useEffect(() => {
        if (paymentInfo) {
            handleConfirmationPayment()
        }
    }, [paymentInfo, refresh]);

    useEffect(() => {
        if (isSuccess && dataSuccess) {
            successCallback(dataSuccess)
            handleCloseModal()
        }
    }, [isSuccess, dataSuccess]);

    const handleConfirmationPayment = () => {
        let body = {
            "merchantKey": merchantKey,
            "bookingRef": paymentInfo.header.bookingRef,
            "paymentCategory": paymentCategory
        }
        getConfirmationBooking(body, token, isDevelop).then((res) => {
            if (res.result) {
                setDataSuccess(res.result)
                setSuccess(true)
            }
            setLoading(false);
        }).catch((e) => {
            setTimeout(() => {
                setRefresh(Date.now());
            }, 2000)
            setLoading(false)
            errorCallback(e)
            console.log(e)
        })
    }

    if (paymentCategory === "CC") {
        return (
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                gap: 2,
                minHeight: "600px",
                position: "relative"
            }}>
                {/* <Box
                    onClick={() => handleClose()}
                    sx={{
                        position: "absolute",
                        top: 2,
                        right: 2,
                        cursor: "pointer"
                    }}>
                    <Close sx={{ fontSize: "2rem" }} />
                </Box> */}
                <Button
                    sx={{
                        bgcolor: "#1A66FF",
                        my: 2,
                        px: 4,
                        py: 1,
                        color: "white",
                        fontWeight: "bold",
                        textTransform: "capitalize",
                        fontSize: 18,
                        ":hover": {
                            bgcolor: "#3377FF"
                        }
                    }}
                    onClick={() => window.open(paymentInfo.apps2PaymentData.paymentUrl, '_blank')}
                >
                    { language ? getWord("bayar") : "Bayar"} CC { language ? getWord("sekarang") : "Sekarang"}
                </Button>
                <Typography sx={{
                    textAlign: "center",
                    width: "70%"
                }}>{ language ? getWord("waktuPembayaran") : "Silahkan melakukan pembayaran sebelum tanggal"} {moment(paymentInfo.apps2PaymentData.expiredAt).format("DD MMM YYYY HH:mm:ss")}</Typography>
            </Box>
        )
    }

    if (paymentCategory === "QRIS") {
        return (
            <Box sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                gap: 2,
                minHeight: "600px",
                position: "relative"
            }}>
                {/* <Box
                    onClick={() => handleClose()}
                    sx={{
                        position: "absolute",
                        top: 2,
                        right: 2,
                        cursor: "pointer"
                    }}>
                    <Close sx={{ fontSize: "2rem" }} />
                </Box> */}
                <Typography sx={{ fontSize: 30, fontWeight: "bold", my: 4 }}>{paymentAlias || paymentMethod}</Typography>
                <Box
                    sx={{
                        position: "relative",
                        backgroundColor: "white",
                        padding: "14px",
                        width: ["80%", "30%"],
                        mx: "auto"
                    }}>
                    <div
                        id="qrCodeQris"
                        ref={downloadRef}
                        style={{
                            position: "relative",
                            backgroundColor: "white",
                            padding: "14px",
                            width: "100%"
                        }}>
                        <QRCodeSVG value={paymentInfo.apps2PaymentData.qrCode} size={"100%"} />
                    </div>
                </Box>
                <Typography sx={{ fontSize: 24, fontWeight: "bold" }}>Rp{thousandSeparator(paymentInfo.header.paymentTotal)},00</Typography>
                <Button
                    onClick={() => {
                        toPng(downloadRef.current, { cacheBust: true, })
                            .then((dataUrl) => {
                                const link = document.createElement('a')
                                link.download = `QRIS_Payment_${Date.now()}.png`
                                link.href = dataUrl
                                link.click()
                            })
                            .catch((err) => {
                                console.log(err)
                            })
                    }}
                    sx={{
                        textTransform: "capitalize",
                        bgcolor: "blue",
                        px: 4,
                        color: "white",
                        ":hover": {
                            bgcolor: "blue"
                        }
                    }}>Download QRIS</Button>
                {
                    paymentInfo.apps2PaymentData.expiredAt ?
                        <Typography sx={{
                            textAlign: "center",
                            width: "70%"
                        }}>{ language ? getWord("waktuPembayaran") : "Silahkan melakukan pembayaran sebelum tanggal"} {moment(paymentInfo.apps2PaymentData.expiredAt).format("DD MMM YYYY HH:mm:ss")}</Typography> : null
                }
            </Box>
        )
    }
    if (paymentCategory === "VIRTUAL ACCOUNT") {
        return (
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 2,
                    justifyContent: "center",
                    alignItems: "center",
                    minHeight: "600px",
                    position: "relative"
                }}>
                {/* <Box
                    onClick={() => handleClose()}
                    sx={{
                        position: "absolute",
                        top: 2,
                        right: 2,
                        cursor: "pointer"
                    }}>
                    <Close sx={{ fontSize: "2rem" }} />
                </Box> */}
                <Typography sx={{ fontSize: 30, fontWeight: "bold", my: 4 }}>{paymentAlias || paymentMethod}</Typography>
                <ClickAwayListener onClickAway={handleTooltipClose}>
                    <div>
                        <Tooltip
                            PopperProps={{
                                disablePortal: true,
                            }}
                            onClose={handleTooltipClose}
                            open={open}
                            disableFocusListener
                            disableHoverListener
                            disableTouchListener
                            title="Berhasil di salin!"
                        >
                            <Stack
                                onClick={() => {
                                    navigator.clipboard.writeText(paymentInfo.apps2PaymentData.vaCode)
                                    setOpen(true)
                                }}
                                direction={"row"}
                                sx={{
                                    px: 6,
                                    py: 1,
                                    bgcolor: "#E6E6E6",
                                    borderRadius: 8,
                                    cursor: "pointer",
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    gap: 2
                                }}
                            >
                                <ContentCopyIcon sx={{ fontSize: 30 }} />
                                <Typography sx={{
                                    fontSize: 30,
                                    fontWeight: "bold"
                                }}>
                                    {paymentInfo.apps2PaymentData.vaCode}
                                </Typography>
                            </Stack>
                        </Tooltip>
                    </div>
                </ClickAwayListener>
                <Typography sx={{ fontSize: 24, fontWeight: "bold" }}>Rp{thousandSeparator(paymentInfo.header.paymentTotal)},00</Typography>
                <Typography sx={{
                    textAlign: "center",
                    width: "70%"
                }}>{ language ? getWord("waktuPembayaran") : "Silahkan melakukan pembayaran sebelum tanggal"} {moment(paymentInfo.apps2PaymentData.expiredAt).format("DD MMM YYYY HH:mm:ss")}</Typography>
            </Box>
        )
    }
    if (paymentMethod === "TUNAI TOP") {
        return (
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 2,
                    justifyContent: "center",
                    alignItems: "center",
                    minHeight: "600px",
                    position: "relative"
                }}>
                {/* <Box
                    onClick={() => handleClose()}
                    sx={{
                        position: "absolute",
                        top: 2,
                        right: 2,
                        cursor: "pointer"
                    }}>
                    <Close sx={{ fontSize: "2rem" }} />
                </Box> */}
                <Typography sx={{ fontSize: 30, fontWeight: "bold", my: 4 }}>{paymentAlias || paymentMethod}</Typography>
                {/* <ClickAwayListener onClickAway={handleTooltipClose}>
                    <div>
                        <Tooltip
                            PopperProps={{
                                disablePortal: true,
                            }}
                            onClose={handleTooltipClose}
                            open={open}
                            disableFocusListener
                            disableHoverListener
                            disableTouchListener
                            title="Berhasil di salin!"
                        >
                            <Stack
                                onClick={() => {
                                    navigator.clipboard.writeText(paymentInfo.apps2PaymentData.vaCode)
                                    setOpen(true)
                                }}
                                direction={"row"}
                                sx={{
                                    px: 6,
                                    py: 1,
                                    bgcolor: "#E6E6E6",
                                    borderRadius: 8,
                                    cursor: "pointer",
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                    gap: 2
                                }}
                            >
                                <ContentCopyIcon sx={{ fontSize: 30 }} />
                                <Typography sx={{
                                    fontSize: 30,
                                    fontWeight: "bold"
                                }}>
                                    {paymentInfo.apps2PaymentData.vaCode}
                                </Typography>
                            </Stack>
                        </Tooltip>
                    </div>
                </ClickAwayListener> */}
                <Typography sx={{ fontSize: 24, fontWeight: "bold" }}>Rp{thousandSeparator(paymentInfo.header.paymentTotal)},00</Typography>
                <Typography sx={{
                    textAlign: "center",
                    width: "70%"
                }}>{ language ? getWord("prosesPembayaran") : "Pembayaran sedang di proses" }</Typography>
            </Box>
        )
    }
    return (
        <Box>

        </Box>
    )
}

export default SelectedPayment;